#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_cblas.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_blas.h>

#include "BayesLine.h"
#include "BayesWave.h"
#include "BayesWaveIO.h" 

#define IM1 2147483563
#define IM2 2147483399
#define AM (1.0/IM1)
#define IMM1 (IM1-1)
#define IA1 40014
#define IA2 40692
#define IQ1 53668
#define IQ2 52774
#define IR1 12211
#define IR2 3791
#define NTAB 32
#define NDIV (1+IMM1/NTAB)
#define eps 1.2e-7
#define RNMX (1.0-eps)
double swap, tempr;
#define SWAP(a,b) {swap=(a);(a)=(b);(b)=swap;}


/* ********************************************************************************** */
/*                                                                                    */
/*                                    Math tools                                      */
/*                                                                                    */
/* ********************************************************************************** */

double gaussian_norm(double min, double max, double sigma)
{
   double sqp2 = 1.2533141373155; //sqrt(pi/2)
   return sqp2*sigma*(erfc(min/LAL_SQRT2/sigma) - erfc(max/LAL_SQRT2/sigma) );
}

void recursive_phase_evolution(double dre, double dim, double *cosPhase, double *sinPhase)
{
   /* Update re and im for the next iteration. */
   double cosphi = *cosPhase;
   double sinphi = *sinPhase;

   double newRe = cosphi + cosphi*dre - sinphi*dim;
   double newIm = sinphi + cosphi*dim + sinphi*dre;

   *cosPhase = newRe;
   *sinPhase = newIm;
   
}

double fourier_nwip(int imin, int imax, double *a, double *b, double *invSn)
{
    int i, j, k;
    double arg;
    /*
    double product;
    double ReA, ReB, ImA, ImB;
     */

    arg = 0.0;
    for(i=imin; i<imax; i++)
    {
        j = i * 2;
        k = j + 1;
        /*
         ReA = a[j]; ImA = a[k];
         ReB = b[j]; ImB = b[k];
         product = ReA*ReB + ImA*ImB;
         arg += product*invSn[i];
         */
        arg += (a[j]*b[j] + a[k]*b[k])*invSn[i];
    }
    return(2.0*arg);    
}

double network_nwip(int imin, int imax, double **a, double **b, double **invSn, double *eta, int NI)
{
   int i;
   double sum=0.0;

   for(i=0; i<NI; i++) sum += fourier_nwip(imin,imax,a[i],b[i],invSn[i])/eta[i];
   return(sum);
}

double network_snr(int imin, int imax, double **h, double **invpsd, double *eta, int NI )
{
   return sqrt( network_nwip(imin,imax,h,h,invpsd,eta,NI) );
}

double detector_snr(int imin, int imax, double *g, double *invpsd, double eta)
{
   return sqrt( fourier_nwip(imin,imax,g,g,invpsd)/eta );
}

/* ********************************************************************************** */
/*                   																					                        */
/*                                  Matrix Routines                                   */
/*											                                                              */
/* ********************************************************************************** */

void matrix_eigenstuff(double **matrix, double **evector, double *evalue, int N)
{
  int i,j;

  // Don't let errors kill the program (yikes)
  gsl_set_error_handler_off ();
  int err=0;

  // Find eigenvectors and eigenvalues
  gsl_matrix *GSLfisher = gsl_matrix_alloc(N,N);
  gsl_matrix *GSLcovari = gsl_matrix_alloc(N,N);
  gsl_matrix *GSLevectr = gsl_matrix_alloc(N,N);
  gsl_vector *GSLevalue = gsl_vector_alloc(N);

  for(i=0; i<N; i++)for(j=0; j<N; j++) gsl_matrix_set(GSLfisher,i,j,matrix[i][j]);

  // sort and put them into evec
  gsl_eigen_symmv_workspace * workspace = gsl_eigen_symmv_alloc (N);
  gsl_permutation * permutation = gsl_permutation_alloc(N);
  err += gsl_eigen_symmv (GSLfisher, GSLevalue, GSLevectr, workspace);
  err += gsl_eigen_symmv_sort (GSLevalue, GSLevectr, GSL_EIGEN_SORT_ABS_ASC);
  err += gsl_linalg_LU_decomp(GSLfisher, permutation, &i);
  err += gsl_linalg_LU_invert(GSLfisher, permutation, GSLcovari);

  if(err>0)
  {
    fprintf(stderr,"BayesWaveMath.c:135: WARNING: singluar matrix, treating matrix as diagonal\n");
    fflush(stderr);
    for(i=0; i<N; i++)for(j=0; j<N; j++)
    {
      evector[i][j] = 0.0;
      if(i==j)
      {
        evector[i][j]=1.0;
        evalue[i]=matrix[i][j];
      }
    }

  }
  else
  {

    //unpack arrays from gsl inversion
    for(i=0; i<N; i++)
    {
      evalue[i] = gsl_vector_get(GSLevalue,i);
      for(j=0; j<N; j++) evector[i][j] = gsl_matrix_get(GSLevectr,i,j);
    }

    for(i=0;i<N-1;i++)for(j=i+1;j<N;j++) gsl_matrix_set(GSLcovari,j,i, gsl_matrix_get(GSLcovari,i,j) );

    //cap minimum size eigenvalues
    for(i=0; i<N; i++) if(evalue[i] < 10.) evalue[i] = 10.;
  }

  gsl_vector_free (GSLevalue);
  gsl_matrix_free (GSLfisher);
  gsl_matrix_free (GSLcovari);
  gsl_matrix_free (GSLevectr);
  gsl_eigen_symmv_free (workspace);
  gsl_permutation_free (permutation);
}

double matrix_jacobian(double **matrix, int N)
{
  int i,j;
  double detJ;

  gsl_matrix *J = gsl_matrix_alloc(N,N);

  for(i=0; i<N; i++) for(j=0; j<N; j++) gsl_matrix_set(J,i,j,matrix[i][j]);

  // get determinant of Jacobian
  gsl_permutation * permutation = gsl_permutation_alloc(N);
  gsl_linalg_LU_decomp(J, permutation, &i);

  detJ = gsl_linalg_LU_det(J, i);

  gsl_matrix_free(J);
  gsl_permutation_free (permutation);
  
  return(detJ);
}

void matrix_multiply(double **matrix, double *vector, double *result, int N)
{
  int i,j;
  for(i=0; i<N; i++)
  {
    result[i] = 0.0;
    for(j=0; j<N; j++)
    {
      result[i] += matrix[i][j]*vector[j];
    }
  }
}

double dot_product(double *a, double *b, int N)
{
  int i;
  double c = 0.0;
  for(i=0; i<N; i++) c+=a[i]*b[i];
  return c;
}


/* ********************************************************************************** */
/*                   																					                        */
/*                                 Fourier Routines                                   */
/*											                                                              */
/* ********************************************************************************** */

void dfour1(double data[], unsigned long nn, int isign)
{
   unsigned long n,mmax,m,j,istep,i;
   double wtemp,wr,wpr,wpi,wi,theta;
   double tempr,tempi, swap;

   n=nn << 1;
   j=1;
   for (i=1;i<n;i+=2) {
      if (j > i) {
         SWAP(data[j],data[i]);
         SWAP(data[j+1],data[i+1]);
      }
      m=n >> 1;
      while (m > 1 && j > m) {
         j -= m;
         m >>= 1;
      }
      j += m;
   }
   mmax=2;
   while (n > mmax) {
      istep=mmax << 1;
      theta=isign*(6.28318530717959/mmax);
      wtemp=sin(0.5*theta);
      wpr = -2.0*wtemp*wtemp;
      wpi=sin(theta);
      wr=1.0;
      wi=0.0;
      for (m=1;m<mmax;m+=2) {
         for (i=m;i<=n;i+=istep) {
            j=i+mmax;
            tempr=wr*data[j]-wi*data[j+1];
            tempi=wr*data[j+1]+wi*data[j];
            data[j]=data[i]-tempr;
            data[j+1]=data[i+1]-tempi;
            data[i] += tempr;
            data[i+1] += tempi;
         }
         wr=(wtemp=wr)*wpr-wi*wpi+wr;
         wi=wi*wpr+wtemp*wpi+wi;
      }
      mmax=istep;
   }
}

void drealft(double data[], unsigned long n, int isign)
{
   void dfour1(double data[], unsigned long nn, int isign);
   unsigned long i,i1,i2,i3,i4,np3;
   double c1=0.5,c2,h1r,h1i,h2r,h2i;
   double wr,wi,wpr,wpi,wtemp,theta;

   theta=3.141592653589793/(double) (n>>1);
   if (isign == 1) {
      c2 = -0.5;
      dfour1(data,n>>1,1);
   } else {
      c2=0.5;
      theta = -theta;
   }
   wtemp=sin(0.5*theta);
   wpr = -2.0*wtemp*wtemp;
   wpi=sin(theta);
   wr=1.0+wpr;
   wi=wpi;
   np3=n+3;
   for (i=2;i<=(n>>2);i++) {
      i4=1+(i3=np3-(i2=1+(i1=i+i-1)));
      h1r=c1*(data[i1]+data[i3]);
      h1i=c1*(data[i2]-data[i4]);
      h2r = -c2*(data[i2]+data[i4]);
      h2i=c2*(data[i1]-data[i3]);
      data[i1]=h1r+wr*h2r-wi*h2i;
      data[i2]=h1i+wr*h2i+wi*h2r;
      data[i3]=h1r-wr*h2r+wi*h2i;
      data[i4] = -h1i+wr*h2i+wi*h2r;
      wr=(wtemp=wr)*wpr-wi*wpi+wr;
      wi=wi*wpr+wtemp*wpi+wi;
   }
   if (isign == 1) {
      data[1] = (h1r=data[1])+data[2];
      data[2] = h1r-data[2];
   } else {
      data[1]=c1*((h1r=data[1])+data[2]);
      data[2]=c1*(h1r-data[2]);
      dfour1(data,n>>1,-1);
   }
}

void Fourier_Scaling(double *ft, int n, double Tobs)
{
   double fix;
   int i;
   
   /* convert Numerical Recipes FFT output into a strain spectral density */
   
   fix = sqrt(Tobs)/((double)n);
   for(i = 0; i < n; i++) ft[i] *= fix;
}

/* ********************************************************************************** */
/*																					                                          */
/*                                       RNGS                                         */
/*											                                                              */
/* ********************************************************************************** */


double gasdev2(long *idum)
{
   double ran2(long *idum);
   static int iset=0;
   static double gset;
   double fac, rsq, v1, v2;

   if(*idum < 0) iset = 0;
   if(iset == 0){
      do{
         v1 = 2.0 * ran2(idum)-1.0;
         v2 = 2.0 * ran2(idum)-1.0;
         rsq = v1*v1+v2*v2;
      } while (rsq >= 1.0 || rsq == 0.0);
      fac=sqrt(-2.0*log(rsq)/rsq);
      gset = v1 * fac;
      iset = 1;
      return(v2*fac);
   } else {
      iset = 0;
      return (gset);
   }
}

double ran2(long *idum)
{
   int j;
   long k;
   static long idum2=123456789;
   static long iy=0;
   static long iv[NTAB];
   double temp;

   if (*idum <= 0) {
      if (-(*idum) < 1) *idum=1;
      else *idum = -(*idum);
      idum2=(*idum);
      for (j=NTAB+7;j>-1;j--) {
         k=(*idum)/IQ1;
         *idum=IA1*(*idum-k*IQ1)-k*IR1;
         if (*idum < 0) *idum += IM1;
         if (j < NTAB) iv[j] = *idum;
      }
      iy=iv[0];
   }
   k=(*idum)/IQ1;
   *idum=IA1*(*idum-k*IQ1)-k*IR1;
   if (*idum < 0) *idum += IM1;
   k=idum2/IQ2;
   idum2=IA2*(idum2-k*IQ2)-k*IR2;
   if (idum2 < 0) idum2 += IM2;
   j=iy/NDIV;
   iy=iv[j]-idum2;
   iv[j] = *idum;
   if (iy < 1) iy += IMM1;
   if ((temp=AM*iy) > RNMX) return RNMX;
   else return temp;
}
