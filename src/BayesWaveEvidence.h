
void average_log_likelihood_via_direct_downsampling(struct Chain *chain, double *ACL, int nPoints);
void average_log_likelihood_via_recursion_downsampling(int M, struct Chain *chain);
void average_log_likelihood_via_ACF(struct Chain *chain);

void SplineThermodynamicIntegration(double *temperature, double *avgLogLikelihood, double *varLogLikelihood, int NC, double *logEvidence, double *varLogEvidence);
void CovarianceThermodynamicIntegration(double *temperature, double **loglikelihood, int M, int NC, char modelname[], double *logEvidence, double *varLogEvidence);
void ThermodynamicIntegration(double *temperature, double **loglikelihood, int M, int NC, char modelname[], double *logEvidence, double *varLogEvidence);
void TrapezoidIntegration(struct Chain *chain, int counts, char modelname[], double *logEvidence, double *varLogEvidence);

void LaplaceApproximation(struct Data *data, struct Model *model, struct Chain *chain, struct Prior *prior, double *logEvidence);
